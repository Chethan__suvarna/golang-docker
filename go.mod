module bitbucket.org/golang-docker

go 1.13

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.12
	github.com/rahmanfadhil/gin-bookstore v0.0.0-20200521154043-2ad4262268f7
)
