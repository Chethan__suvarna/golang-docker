package models

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var DB *gorm.DB

func ConnectDataBase() {
	dataBase, err := gorm.Open("sqlite3", "test.db")

	if err != nil {
		panic("Not able to connect to database")
	}

	dataBase.AutoMigrate(&Book{})
	DB = dataBase
}
